module gitlab.com/chandappa/log

go 1.15

require (
	github.com/golang/glog v0.0.0-20160126235308-23def4e6c14b
	github.com/natefinch/lumberjack v2.0.0+incompatible
	github.com/spf13/cobra v1.1.1
	go.uber.org/zap v1.16.0
	google.golang.org/grpc v1.29.1
	gopkg.in/natefinch/lumberjack.v2 v2.0.0 // indirect
)
